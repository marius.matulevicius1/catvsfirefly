﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class BuyPowerupScript : MonoBehaviour {

    public GameObject dogObject;
    TextMeshProUGUI dogText;
	// Use this for initialization
	void Start () {

        if (!PlayerPrefs.HasKey("DogTimer"))
        {
            PlayerPrefs.SetInt("DogTimer", 1);
        }

        dogText = dogObject.GetComponent<TextMeshProUGUI>();
        dogText.SetText(PlayerPrefs.GetInt("DogTimer").ToString());


    }

    public void buyDogButton()
    {
        if(PlayerPrefs.GetFloat("Coins") >= 100)
        {
            int temp = PlayerPrefs.GetInt("DogTimer");
            temp += 1;
            PlayerPrefs.SetInt("DogTimer", temp);
            dogText.SetText(temp.ToString());

            reduceMoneyAmount(100);

        }
       
    }

    void reduceMoneyAmount(float price)
    {
        float coins = PlayerPrefs.GetFloat("Coins");
        coins -= price;
        PlayerPrefs.SetFloat("Coins", coins);
    }
}
