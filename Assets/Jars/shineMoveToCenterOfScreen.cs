﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shineMoveToCenterOfScreen : MonoBehaviour {
    public Vector3 positionToMoveTo = new Vector3(2.54f, 0.83f, 0);
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, positionToMoveTo, 2 * Time.smoothDeltaTime);
        if (transform.position == positionToMoveTo)
            Destroy(gameObject);
    }
}
