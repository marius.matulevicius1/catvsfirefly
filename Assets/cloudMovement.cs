﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudMovement : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private Vector3 origPos = new Vector3(-18.61f, 0, 0);
    // Use this for initialization
    void Start()
    {
        origPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * speed);
        if (transform.position.x > 14.7)
        {
            transform.position = origPos;
        }
    }
}
