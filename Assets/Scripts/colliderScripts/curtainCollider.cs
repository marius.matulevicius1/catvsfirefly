﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class curtainCollider : MonoBehaviour {

    public bool playerInCenter = false;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInCenter = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInCenter = false;

        }
    }
}
