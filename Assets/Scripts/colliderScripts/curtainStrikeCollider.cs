﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class curtainStrikeCollider : MonoBehaviour {

    Cat cat;
    BoxCollider2D bColl;

    float currentTime = 0;
    public float Delay = 15;



    private void Start()
    {
        bColl = GetComponent<BoxCollider2D>();
    }


    private void Update()
    {
        if (!bColl.isActiveAndEnabled)
        {
            currentTime += Time.deltaTime;

            if (currentTime >= Delay)
            {
                bColl.enabled = true;
                currentTime = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            getCat();
            if(cat.Current_Cat_State == Cat.CatStates.FollowPlayer || cat.Current_Cat_State == Cat.CatStates.Idle)
            {
                cat.Current_Cat_State = Cat.CatStates.curtainStrike;
                bColl.enabled = false;
            }
        }
    }


    void getCat()
    {
        if(cat == null)
        {
            cat = GameManager.Instance.getCatObject().GetComponent<Cat>();
        }
    }
}
