﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closeAttackCollider : MonoBehaviour {
    Cat cat;
	// Use this for initialization
	void Start () {
        //cat = GetComponentInParent<Cat>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(cat == null)
        {
            cat = GameManager.Instance.getCatObject().GetComponent<Cat>();
        }
        if ((cat.Current_Cat_State == Cat.CatStates.Idle || cat.Current_Cat_State == Cat.CatStates.FollowPlayer) && collision.gameObject.tag == "Player") 
        cat.Current_Cat_State = Cat.CatStates.closeAttack;
    }
}
