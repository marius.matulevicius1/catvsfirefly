﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicJumpCollider : MonoBehaviour {

    public GameObject catObject;
    Cat cat;
    // Use this for initialization
    void Start()
    {
        cat = catObject.GetComponent<Cat>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
            if (cat.Current_Cat_State == Cat.CatStates.FollowPlayer || cat.Current_Cat_State == Cat.CatStates.Idle)
            {
                cat.Current_Cat_State = Cat.CatStates.BasicJumpUp;
            }
    }
}
