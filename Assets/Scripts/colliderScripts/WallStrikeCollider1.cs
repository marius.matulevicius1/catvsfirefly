﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallStrikeCollider1 : MonoBehaviour
{

    GameObject player;
    GameObject catObject;
    Cat cat;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Cat")
        {
            getCatObject();
            getCat();
            getPlayerObject();


            if (Mathf.Abs((catObject.transform.position - player.transform.position).x) <= 2.5f)
            {
                if (cat.Current_Cat_State == Cat.CatStates.FollowPlayer || cat.Current_Cat_State == Cat.CatStates.Idle)
                {
                    cat.cornerJumpOnHold = true;
                }
            }
        }

    }

    private void getCatObject()
    {
        if (catObject == null)
        {
            catObject = GameManager.Instance.getCatObject();
        }
    }

    private void getCat()
    {
        if (cat == null)
            cat = catObject.GetComponent<Cat>();
    }

    private void getPlayerObject()
    {
        if (player == null)
            player = GameManager.Instance.getPlayerObject();
    }


}
