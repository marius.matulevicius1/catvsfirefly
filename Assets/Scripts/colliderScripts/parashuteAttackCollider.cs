﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parashuteAttackCollider : MonoBehaviour {

    CircleCollider2D coll;
    Cat cat;
    public float howOften = 15;
    public GameObject cornerCollider;
    float currentTime = 0;
	// Use this for initialization


	void Start () {
        coll = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {

        currentTime += Time.deltaTime;

        if(currentTime >= howOften)
        {
            coll.enabled = true;
            currentTime = 0;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            getCatObject();
            if(cat.Current_Cat_State == Cat.CatStates.Idle || cat.Current_Cat_State == Cat.CatStates.FollowPlayer)
            cat.Current_Cat_State = Cat.CatStates.ParashuteAttack;

            if(cornerCollider == null)
            {
                cornerCollider = GameManager.Instance.getParashuteCornerCollider();
            }
            cornerCollider.SetActive(true);
        }
    }

    void getCatObject()
    {
        if(cat == null)
            cat = GameManager.Instance.getCatObject().GetComponent<Cat>();
    }
}
