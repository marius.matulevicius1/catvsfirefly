﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu ]
public class jar : ScriptableObject {
    public int numberOfBug;

    public string nameOfBugInside;

    public Sprite jarSprite;
    public Sprite bugSprite;
    public Sprite openJar;

    public GameObject bugGoUpPrefab;
    
	
    public bool buyJar()
    {
        float coins = PlayerPrefs.GetFloat("Coins");
        if (coins >= 50)
        {
            coins -= 50;
            PlayerPrefs.SetFloat("Coins", coins);
            PlayerPrefs.SetInt(numberOfBug.ToString(), 1);
            return true;
        }
        return false;
    }

}
