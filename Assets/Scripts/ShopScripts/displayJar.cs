﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class displayJar : MonoBehaviour {

    public Text moneyText;

    public jar thisJar;

    Animator anim;
    SpriteRenderer rend;

    //public GameObject platformCollider;
    public GameObject breakJar;
    public GameObject breakJarLid;

    public GameObject cat;

    public Vector3 originalJarLocation;

    Rigidbody2D rb;

    Quaternion rotation;

    void Start () {
        anim = GetComponent<Animator>();
        setImageOfJar();
        setMoneyText();

        originalJarLocation = transform.position;
        rotation = transform.rotation;

        rb = GetComponent<Rigidbody2D>();

    }

    private void Update()
    {
        // Code for OnMouseDown in the iPhone. Unquote to test.
        RaycastHit hit = new RaycastHit();
        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
            {
                // Construct a ray from the current touch coordinates
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                if (Physics.Raycast(ray, out hit))
                {
                    hit.transform.gameObject.SendMessage("OnMouseDown");
                }
            }
        }
    }

    

    private void OnMouseDown()
    {
        Debug.Log("Helo");
        openJar();
    }

    void setMoneyText()
    {
        moneyText.text = "money: " + PlayerPrefs.GetFloat("Coins").ToString();

    }

    void setImageOfJar()
    {
        if (PlayerPrefs.GetInt(thisJar.numberOfBug.ToString()) == 1)
        {
            anim.enabled = false;
            rend = GetComponent<SpriteRenderer>();
            rend.sprite = thisJar.openJar;
        }
        else
            anim.SetBool("FullJar", true);

    }

    public void openJar()
    {
        if (PlayerPrefs.GetInt(thisJar.numberOfBug.ToString()) != 1)
        {
            if (thisJar.buyJar())
            {
                instantiateCat();
            }
            //setImageOfJar();
            setMoneyText();
        }
        else
        {
           PlayerPrefs.SetInt(thisJar.numberOfBug.ToString(), 0);
           PlayerPrefs.SetInt("PlayerSelection", thisJar.numberOfBug);
        }
    }

    void instantiateCat()
    {
        if (gameObject.layer != 8 && gameObject.layer != 11 && gameObject.layer != 15 && gameObject.layer != 19)
        {
            Vector3 catPos = new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z);
            Instantiate(cat, catPos, transform.rotation);
            moveJar(true);

        }
        else
        {
            Vector3 catPos = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z);
            GameObject catTemp = Instantiate(cat, catPos, transform.rotation);
            catTemp.transform.localScale = new Vector3(catTemp.transform.localScale.x * -1, catTemp.transform.localScale.y, catTemp.transform.localScale.z);
            moveJar(false);
        }
    }

    void moveJar(bool right)
    { 
        if(right)
        rb.AddRelativeForce(Vector2.right * 5000);
        else
        rb.AddRelativeForce(Vector2.left * 5000);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "groundCollider")
        {
            GameObject brJar = Instantiate(breakJar, new Vector3(transform.position.x , transform.position.y + 1, transform.position.z), transform.rotation);
            GameObject brJarLid = Instantiate(breakJarLid, new Vector3(transform.position.x +1, transform.position.y +1, transform.position.z), transform.rotation);
            brJar.GetComponent<Rigidbody2D>().AddForce(Vector2.left * 300);
            brJarLid.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 300);
            Instantiate(thisJar.bugGoUpPrefab, transform.position, Quaternion.identity);

            transform.rotation = rotation;
            transform.position = originalJarLocation;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = 0f;
            rb.Sleep();
            setImageOfJar();

        }
    }


}
