﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBird : MonoBehaviour {
    //y pos 4-1
    // Use this for initialization
    public GameObject bird;
    public GameObject Sign;
    float seconds = 0f;
    float gameTimer = 0f;
    float timeBetweenBirdSpawns = 3;
    float originalTimeBetweenBirdSpawns;

	void Start () {
        originalTimeBetweenBirdSpawns = timeBetweenBirdSpawns;
	}


    private void OnDisable()
    {
        resetTimes();
    }

    // Update is called once per frame
    void Update () {
        timerCount();
        
	}

    private void timerCount()
    {
        if (seconds == timeBetweenBirdSpawns)
        {
            seconds = 0;
            timeBetweenBirdSpawns = Random.Range(10, 15);
            spawnSign();
        }
        if (Time.time > gameTimer + 1)
        {
            gameTimer = Time.time;
            seconds++;
        }
    }

    private void spawnSign()
    {
        float randomYPos = Random.Range(-6f, -10f);
        int side = Random.Range(0,2);
        if (side == 0)   
            Instantiate(Sign, new Vector3(-8f, randomYPos, 0f), transform.rotation);     
        else      
            Instantiate(Sign, new Vector3(8f, randomYPos, 0f), transform.rotation);
       
        StartCoroutine(waitSecondsAndSpawnBird(randomYPos, side));
    }

    IEnumerator waitSecondsAndSpawnBird( float pos, int side)
    {
        yield return new WaitForSeconds(2f);
        if (side == 0)
        {
            Instantiate(bird, new Vector3(-15f, pos, 0f), transform.rotation);
        }
        else
        {
            Instantiate(bird, new Vector3(15f, pos, 0f), transform.rotation);
        }

    }

    public void resetTimes()
    {
        seconds = 0f;
        gameTimer = 0f;
        timeBetweenBirdSpawns = originalTimeBetweenBirdSpawns;
    }
}
