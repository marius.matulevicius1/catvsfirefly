﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogBonePowerUp : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.getCatObject().GetComponent<Cat>().isDogBarking = true;
            Destroy(gameObject);
        }
    }
}
