﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectionScript : MonoBehaviour {

    public GameObject jarsReveal;
    Animator jarsAnim;

    private void Start()
    {
        jarsAnim = jarsReveal.GetComponent<Animator>();    
    }

    public void startJarsAnimation()
    {
        jarsAnim.SetBool("jars", true);
    }

}
