﻿using UnityEngine;

public class birdScript : MonoBehaviour {

    bool right;
    // Update is called once per frame
    private void Start()
    {
        if (transform.position.x == 15)
        {
            right = true;
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
    }

    void Update () {
        if (!right)
            transform.Translate(Vector3.right * Time.deltaTime * 30f);
        else
        {
            transform.Translate(Vector3.left * Time.deltaTime * 30f);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bound")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Sign")
        {
            Destroy(collision.gameObject);
        }

    }
}
