﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class sofaScript : MonoBehaviour {

    public GameObject tusCat;
    public Button playButton;
    public Button TheRoomButton;


    public void TusOn()
    {
        if (gameObject.GetComponent<SpriteRenderer>().enabled)
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            tusCat.SetActive(true);
            playButton.enabled = true;
            TheRoomButton.enabled = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            tusCat.SetActive(false);
        }
    }
}
