﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawning : MonoBehaviour {

    public GameObject DogBone;

    float currentTime = 0;
    public float DogBonedelay = 20;

    float boundX = 8;

	
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime;

        if(currentTime >= DogBonedelay)
        {
            currentTime = 0;
            spawnDogBone();
        }
	}

    void spawnDogBone()
    {
        Instantiate(DogBone, new Vector3(Random.Range(-boundX, boundX), Random.Range(-12, -6), 0f), transform.rotation);
    }
}
