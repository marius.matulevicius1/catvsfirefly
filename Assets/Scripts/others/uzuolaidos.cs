﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uzuolaidos : MonoBehaviour {
    cameraMoveToPlayPos cam;

    public GameObject dont;

    private void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<cameraMoveToPlayPos>();
    }

    public void OpenScene()
    {
        dont.SetActive(true);
        cam.OpenPlayScene();
    }
}
