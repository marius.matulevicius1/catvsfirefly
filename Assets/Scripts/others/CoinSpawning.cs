﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawning : MonoBehaviour {

    public GameObject theCoin;
    float boundX = 8;
    //float boundY = -6.53f;

    GameObject spawnedCoin;

    private void Start()
    {
        spawnedCoin = Instantiate(theCoin, new Vector3(Random.Range(-boundX, boundX), Random.Range(-6f, -14f), 0f), transform.rotation);

    }
    private void FixedUpdate()
    {
        spawnCoin();
        
    }

    private void spawnCoin()
    {
        if(spawnedCoin == null)
           spawnedCoin = Instantiate(theCoin, new Vector3(Random.Range(-boundX, boundX), Random.Range(-12, -6), 0f), transform.rotation);
    }

}
