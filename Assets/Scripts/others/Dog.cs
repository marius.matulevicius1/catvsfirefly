﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour {

    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void startCountingTime()
    {
        PlayerPrefs.SetInt("DogTimer", 1);
        anim.SetBool("dogBark", true);
        StartCoroutine(dog());
    }


    public void DogDisable()
    {
        Cat cat = GameManager.Instance.getCatObject().GetComponent<Cat>();
        cat.Current_Cat_State = Cat.CatStates.WalkBackToCenter;
        cat.DontRotate = false;
        cat.isDogBarking = false;
        cat.anim.SetBool("dogBark", false);
        gameObject.SetActive(false);
    }

    IEnumerator dog()
    {
        yield return new WaitForSeconds(PlayerPrefs.GetInt("DogTimer"));

        anim.SetBool("dogBark", false);

    }



}
