﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Test : MonoBehaviour {



    //---------- ONLY NECESSARY FOR ASSET PACKAGE INTEGRATION: ----------//

#if UNITY_IOS
private static string gameId = "1725997";
#elif UNITY_ANDROID
    private static string gameId = "1725998";
#endif

//-------------------------------------------------------------------//

[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{
    Button m_Button;

    public string placementId = "rewardedVideo";

    void Start()
    {
        m_Button = GetComponent<Button>();
        if (m_Button) m_Button.onClick.AddListener(ShowAd);

        //---------- ONLY NECESSARY FOR ASSET PACKAGE INTEGRATION: ----------//

        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(gameId, true);
        }

        //-------------------------------------------------------------------//

    }

    void Update()
    {
        if (m_Button) m_Button.interactable = Advertisement.IsReady(placementId);
    }

    void ShowAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show(placementId, options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");

        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}
}
