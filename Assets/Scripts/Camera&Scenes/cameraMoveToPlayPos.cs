﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class cameraMoveToPlayPos : MonoBehaviour
{

    public float cameraMoveSpeed = 5;

    bool MoveCameraToPlayBool = false;
    bool moveCameraToCollection = false;
    bool moveCameraFromCollection = false;

    public GameObject playButtonObject;
    public GameObject collectionButtonObject;
    public GameObject MoveBackFromCollectionsButton;

    public GameObject SofaObject;
    public GameObject sofaTusObject;
    public GameObject UzuolaidaObject;

    public GameObject cameraRotationObject;

    Animator SofaAnimator;
    Animator UzuolaidaAnimator;

    public Vector3 originalCameraPsoition;
    public Quaternion originalCameraRotation;


    private void Start()
    {
        originalCameraRotation = transform.rotation;
        SofaAnimator = SofaObject.GetComponent<Animator>();
        UzuolaidaAnimator = UzuolaidaObject.GetComponent<Animator>();



    }

    void LateUpdate()
    {
        if (MoveCameraToPlayBool)
        {
            moveCameraToPlayPosition();
        }

        if (moveCameraToCollection)
        {
            moveCameraToCollectionArea();
        }

        if (moveCameraFromCollection)
        {
            moveCameraToOriginalPosition();
        }
    }

    void moveCameraToPlayPosition()
    {
        if (transform.position.x != -8.14f)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-8.14f, 1.34f, -0.75f), cameraMoveSpeed * .7f * Time.smoothDeltaTime);
        }
    }

    void moveCameraToCollectionArea()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(-57.06f, -3.6f, -20.98f), cameraMoveSpeed * Time.smoothDeltaTime);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, cameraRotationObject.transform.rotation, 20 * Time.deltaTime);

        if (transform.position == new Vector3(-57.06f, -3.6f, -20.98f))
        {
            moveCameraToCollection = false;
            MoveBackFromCollectionsButton.GetComponent<Button>().enabled = true;
        }

    }

    void moveCameraToOriginalPosition()
    {
        transform.position = Vector3.MoveTowards(transform.position, originalCameraPsoition, cameraMoveSpeed * Time.smoothDeltaTime);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, originalCameraRotation, 20 * Time.deltaTime);

        if (transform.position == originalCameraPsoition)
        {
            moveCameraFromCollection = false;
            //playButtonObject.GetComponent<Button>().enabled = true;
            //collectionButtonObject.GetComponent<Button>().enabled = true;
        }

    }

    public void playButton()
    {
        MoveCameraToPlayBool = true;
        SofaAnimator.SetBool("Sofa", false);
        UzuolaidaAnimator.SetBool("Uzuolaidos", true);
        playButtonObject.GetComponent<Button>().enabled = false;
        collectionButtonObject.GetComponent<Button>().enabled = false;

    }



    public void collectionButton()
    {
        moveCameraToCollection = true;
        SofaAnimator.SetBool("Sofa", false);

        playButtonObject.GetComponent<Button>().enabled = false;
        collectionButtonObject.GetComponent<Button>().enabled = false;

    }

    public void moveBackFromCollectionButton()
    {
        moveCameraFromCollection = true;
        MoveBackFromCollectionsButton.GetComponent<Button>().enabled = false;
        SofaAnimator.SetBool("Sofa", true);
    }

    public void OpenPlayScene()
    {
        SceneManager.LoadScene("test");
    }

}
