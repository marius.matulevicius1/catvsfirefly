﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class virtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    public Image bgImg;
    public Image joystickImg;
    public GameObject bg;
    public GameObject head;
    private Vector3 inputVector;
    public float distanceOfBaseAndKnob;



    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform
                                                                    , ped.position
                                                                    , ped.pressEventCamera
                                                                    , out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x * 2 , 0, pos.y * 2 );
            inputVector = (inputVector.magnitude > 1f) ? inputVector.normalized : inputVector;

            joystickImg.rectTransform.anchoredPosition =
                new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x / 3)
                                            , inputVector.z * (bgImg.rectTransform.sizeDelta.y / 3f));

        }
        if (Vector3.Distance(joystickImg.transform.position, bgImg.transform.position) > 16f)
        {
            Vector2 direction = ped.position - new Vector2( joystickImg.transform.position.x
                                                           ,joystickImg.transform.position.y);
            bgImg.transform.Translate(direction * 10 * Time.deltaTime);
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        bg.SetActive(true);
        OnDrag(ped);
        bgImg.transform.position = ped.position;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        bg.SetActive(false);
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;

    }

    private void OnDisable()
    {
        bg.SetActive(false);
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }

    public float Horizontal()
    {
        if(inputVector.x != 0)
        {
            return inputVector.x;
        }
        else
        {
            return Input.GetAxis("Horizontal");
        }
    }
    public float Vertical()
    {
        if (inputVector.z != 0)
        {
            return inputVector.z;
        }
        else
        {
            return Input.GetAxis("Vertical");
        }
    }

    public Vector2 vector()
    {
        return inputVector;
    }
}
