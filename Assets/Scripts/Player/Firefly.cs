﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firefly : MonoBehaviour {
    public float moveSpeed;
    public virtualJoystick joystick;
    private SpriteRenderer spr;
    private bool FacingRight = false;
    Animator anim;


    void Start () {
        anim = GetComponent<Animator>();
        spr = GetComponent<SpriteRenderer>();

    }


    private void OnEnable()
    {
        if (!PlayerPrefs.HasKey("PlayerSelection"))
        {
            PlayerPrefs.GetInt("PlayerSelection", 0);
        }

    }
    


    void Update () {

        Movement();
        flip();
        switch (PlayerPrefs.GetInt("PlayerSelection"))
        {
            case 0:
                anim.SetBool("Fly", true);
                break;
            case 1:
                anim.SetBool("freakyFireFly", true);
                break;

        }
    }


    private void flip()
    {
        //atsisuka i desine
        if (joystick.Horizontal() < 0)
        {
            if (!FacingRight)
            {
                FacingRight = true;
                spr.flipX = false;
            }
        }
        else // atsisuka i kaire
        if (joystick.Horizontal() > 0)
        {
            if (FacingRight)
            {
                FacingRight = false;
                spr.flipX = true;
            }
        }
    }

    private void Movement()
    {
        Vector3 moveVector = new Vector2(joystick.Horizontal(), joystick.Vertical()).normalized;
        transform.Translate(moveVector * moveSpeed * Time.deltaTime);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -9.14f, 8.06f)
                                        ,Mathf.Clamp(transform.position.y, -15.19f, -5.41f)
                                        ,transform.position.z);

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Coin")
        {
            Debug.Log("Coin");
            GameManager.Instance.pickUpCoin();
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "Cat")
        {
            GameManager.Instance.GameOver();
            gameObject.SetActive(false);
        }


    }

}
