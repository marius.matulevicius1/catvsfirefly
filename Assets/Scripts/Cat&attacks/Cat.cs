﻿using System.Collections;
using UnityEngine;

public class Cat : MonoBehaviour
{
    public enum CatStates
    {
        Idle,
        FollowPlayer,
        shootFurrBall,
        TapWithPaw,
        jumpUpToTopOfScreen,
        WalkBackToCenter,
        shootGuns,
        cornerJump,
        BasicJumpUp,
        closeAttack,
        ParashuteAttack,
        curtainStrike,
        inProgressOfSomething,
        DogBarking,
        GameOver
    }

    public CatStates Current_Cat_State;

    public Animator anim;
    public Rigidbody2D rb;

    public GameObject FurrBall;
    public GameObject tapWithPawCollider;
    public GameObject closeAttackCollider;
    public GameObject basicJumpUpCollider;
    public GameObject jumpUpToTopOfScreenCollider;
    public GameObject parashuteAttackCollider;
    GameObject curtainStrikeCollider;
    GameObject wallStrikeCollider1;
    GameObject wallStrikeCollider2;

    public GameObject Player;
    public GameObject smokeParticles;



    public bool arrivetToPlace = true;
    public bool atejoPrieKampo;
    public bool atejoPrieKampoCurtain;
    public bool checkForPosition = true;
    public bool canMove = true;
    public bool DontRotate;
    public bool curtainStrikeRight;
    public bool isDogBarking = false;
    private bool FacingRight = true;
    private bool jumpToCurtain = true;
    private bool gameOver = false;
    private bool inCorner = false;
    public bool cornerJumpOnHold = false;


    public float bounx1 = -3.7f;
    public float bounx2 = 3.8f;
    //public float catJumpTroughScreenForce;
    public float catJumpUpForce;
    public float catMoveSpeed = 5f;
    public float positionToWalkToX;
    public float maxXPos1;
    public float maxXPos2;
    //public float StandardYpos = -4.65f;
    private float gameTimer;

    private int seconds;


    private void OnEnable()
    {
        Current_Cat_State = CatStates.Idle;
        arrivetToPlace = true;
        DontRotate = false;
        checkForPosition = true;
        seconds = 0;
    }


    private void Start()
    {
        maxXPos1 = GameManager.Instance.getMaxXPos1();
        maxXPos2 = GameManager.Instance.getMaxXPos2();
        Player = GameManager.Instance.getPlayerObject();
        //spr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Current_Cat_State = CatStates.FollowPlayer;
        //StandardYpos = transform.position.y;
        positionToWalkToX = GameManager.Instance.catOriginalPosition.x;

    }


    public void PlaySmokeParticles()
    {
        smokeParticles.SetActive(true);
    }

    private void Update()
    {
        //TESTAVIMUI
        if (Input.GetKeyDown(KeyCode.Space)) Current_Cat_State = CatStates.cornerJump;
        //---------------------------------------------------

        if (!DontRotate)
            lookAtPlayer();

        controllColliders();

        if (!gameOver)
            doCurrentState();
    }

    void controllColliders()
    {
        if (Current_Cat_State == CatStates.Idle || Current_Cat_State == CatStates.FollowPlayer)
        {
            enableColliders();
        }
        else
        {
            disableColliders();
        }
    }


    private void doCurrentState()
    {
        if (isDogBarking && (Current_Cat_State == CatStates.Idle || Current_Cat_State == CatStates.FollowPlayer))
        {
            Current_Cat_State = CatStates.DogBarking;
        }

        if (cornerJumpOnHold && (Current_Cat_State == CatStates.Idle || Current_Cat_State == CatStates.FollowPlayer))
        {
            Current_Cat_State = CatStates.cornerJump;
            cornerJumpOnHold = false;
        }

        switch (Current_Cat_State)
        {
            case CatStates.Idle:
                Idle();
                break;
            case CatStates.FollowPlayer:
                FollowPlayer();
                break;
            case CatStates.shootFurrBall:
                shootFurrBall();
                break;
            case CatStates.TapWithPaw:
                TapWithPaw();
                break;
            case CatStates.WalkBackToCenter:
                WalkBackToCenter();
                break;
            case CatStates.jumpUpToTopOfScreen:
                jumpUpToTopOfScreen();
                break;
            case CatStates.cornerJump:
                teleportToCorner();
                break;
            case CatStates.inProgressOfSomething:
                break;
            case CatStates.BasicJumpUp:
                catBasicJumpUp();
                break;
            case CatStates.closeAttack:
                doCloseAttack();
                break;
            case CatStates.ParashuteAttack:
                doParashuteAttack();
                break;
            case CatStates.curtainStrike:
                doCurtainStrike();
                break;
            case CatStates.GameOver:
                doGameOver();
                break;
            case CatStates.DogBarking:
                doDogBarking();
                break;
        }
    }

    void doGameOver()
    {
        anim.SetBool("gameOver", true);
        gameOver = true;
        positionToWalkToX = transform.position.x;
    }

    void doDogBarking()
    {
        DontRotate = true;
        if (!FacingRight)
        {
            FacingRight = true;

            var scale = transform.localScale;
            scale = new Vector3(scale.x * -1, scale.y, scale.z);
            transform.localScale = scale;
        }
        anim.SetBool("dogBark", true);
    }

    public void dogSetActive()
    {
        GameManager.Instance.Dog.SetActive(true);
    }


    public void disableColliders()
    {
        getCurtainStrikeCollider();
        getWallStrikeColliders();
        tapWithPawCollider.SetActive(false);
        closeAttackCollider.SetActive(false);
        basicJumpUpCollider.SetActive(false);
        jumpUpToTopOfScreenCollider.SetActive(false);
        parashuteAttackCollider.SetActive(false);
        curtainStrikeCollider.SetActive(false);
        wallStrikeCollider1.SetActive(false);
        wallStrikeCollider2.SetActive(false);

    }

    public void enableColliders()
    {
        getCurtainStrikeCollider();
        getWallStrikeColliders();
        tapWithPawCollider.SetActive(true);
        closeAttackCollider.SetActive(true);
        basicJumpUpCollider.SetActive(true);
        jumpUpToTopOfScreenCollider.SetActive(true);
        parashuteAttackCollider.SetActive(true);
        curtainStrikeCollider.SetActive(true);
        wallStrikeCollider1.SetActive(true);
        wallStrikeCollider2.SetActive(true);
    }

    void getCurtainStrikeCollider()
    {
        if (curtainStrikeCollider == null)
        {
            curtainStrikeCollider = GameManager.Instance.getCurtainStrikeCollider();
        }
    }

    void getWallStrikeColliders()
    {
        if (wallStrikeCollider1 == null || wallStrikeCollider2 == null)
        {
            wallStrikeCollider1 = GameManager.Instance.getWallStrikeCollider1();
            wallStrikeCollider2 = GameManager.Instance.getWallStrikeCollider2();

        }
    }

    public void sucsessfullJumpToCurtain()
    {
        anim.SetBool("curtainStrike", false);
        Current_Cat_State = CatStates.inProgressOfSomething;
    }

    void doCurtainStrike()
    {
        DontRotate = true;
        if (inCorner)
        {
            anim.SetBool("curtainStrike", true);
            curtainStrikeJump();
        }
        else
            walkToCornerCurtainStrike();


    }

    void curtainStrikeJump()
    {
        DontRotate = true;
        //-7.21f 7.21f

        if (curtainStrikeRight)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(7.2f, transform.position.y, transform.position.z), 20 * Time.deltaTime);

            if (!FacingRight)
            {
                FacingRight = true;

                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }

            if (transform.position == new Vector3(7.2f, transform.position.y, transform.position.z))
            {

                if (jumpToCurtain)
                {
                    anim.SetBool("climbCurtain", true);
                    anim.SetBool("curtainStrike", false);
                    Current_Cat_State = CatStates.inProgressOfSomething;
                    inCorner = false;

                }
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-8.52f, transform.position.y, transform.position.z), 20 * Time.deltaTime);

            if (FacingRight)
            {
                FacingRight = false;

                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }

            if (transform.position == new Vector3(-8.52f, transform.position.y, transform.position.z))
            {

                if (jumpToCurtain)
                {
                    anim.SetBool("climbCurtain", true);
                    anim.SetBool("curtainStrike", false);
                    Current_Cat_State = CatStates.inProgressOfSomething;
                    inCorner = false;
                }
            }
        }
    }

    public void addForceToLanding()
    {
        if (FacingRight)
        {
            rb.AddForce(Vector2.right * 1000);
        }
        else
        {
            rb.AddForce(Vector2.left * 1000);

        }
    }

    public void curtainLandUnsucsessfull()
    {
        anim.SetBool("climbCurtain", false);
        anim.SetBool("curtainLandUnsucsessfull", false);
        Current_Cat_State = CatStates.FollowPlayer;
        DontRotate = false;
    }

    public void teleportToCenter()
    {
        transform.position = GameManager.Instance.catOriginalPosition;
    }

    void doParashuteAttack()
    {
        if (!FacingRight)
        {
            FacingRight = true;

            var scale = transform.localScale;
            scale = new Vector3(scale.x * -1, scale.y, scale.z);
            transform.localScale = scale;
        }


        anim.SetBool("ParashuteAttack", true);
        Current_Cat_State = CatStates.inProgressOfSomething;
        DontRotate = true;
    }

    public void climbUpWindow()
    {
        transform.position = GameManager.Instance.catOriginalPosition;
        anim.SetBool("ParashuteAttack", false);
        resetCatVelocity();
        DontRotate = false;
    }

    void resetCatVelocity()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = 0;

    }

    private void doCloseAttack()
    {
        anim.SetBool("catCloseAttack", true);
    }

    public void setState()
    {
        if (Mathf.Abs(Player.transform.position.x - transform.position.x) > 2.5 &&
            Mathf.Abs(Player.transform.position.y - transform.position.y) > 2.5) // shoot furr ball 
            Current_Cat_State = CatStates.shootFurrBall;
    }

    private float timerCount()
    {
        if (seconds == 4)
        {
            seconds = 0;
            setState();
        }

        if (Time.time > gameTimer + 1)
        {
            gameTimer = Time.time;
            seconds++;
        }

        return seconds;
    }

    private void Idle()
    {
        Current_Cat_State = CatStates.FollowPlayer;
    }

    private void FurrBallAttack()
    {
        var furrBall = Instantiate(FurrBall, transform.position, transform.rotation);
        Physics2D.IgnoreCollision(furrBall.GetComponent<CircleCollider2D>(), GetComponent<BoxCollider2D>());
        var moveDir = (Player.transform.position - transform.position).normalized;
        furrBall.GetComponent<Rigidbody2D>().velocity = moveDir * 20f;

        Current_Cat_State = CatStates.FollowPlayer;
    }


    private void FollowPlayer()
    {
        // Debug.Log(Mathf.Abs((Player.transform.position.x - transform.position.x)));
        if (canMove)
        {
            if (Mathf.Abs(Player.transform.position.x - transform.position.x) > 3f && arrivetToPlace)
            {
                DontRotate = true;
                setPositionXToWalkTo();
            }

            if (positionToWalkToX != transform.position.x)
            {
                arrivetToPlace = false;
                anim.SetBool("CatWalk", true);
                transform.position = Vector3.MoveTowards(transform.position
                    , new Vector3(positionToWalkToX, transform.position.y, transform.position.z)
                    , catMoveSpeed * Time.deltaTime);

                if (positionToWalkToX < transform.position.x && FacingRight)
                {
                    FacingRight = false;

                    var scale = transform.localScale;
                    scale = new Vector3(scale.x * -1, scale.y, scale.z);
                    transform.localScale = scale;
                }

                if (positionToWalkToX > transform.position.x && !FacingRight)
                {
                    FacingRight = true;

                    var scale = transform.localScale;
                    scale = new Vector3(scale.x * -1, scale.y, scale.z);
                    transform.localScale = scale;
                }
            }

            if (positionToWalkToX == transform.position.x)
            {
                DontRotate = false;
                anim.SetBool("CatWalk", false);
                if (timerCount() > 3f)
                {
                    setState();
                    seconds = 0;
                }
            }
        }
    }

    public void lookAtPlayer()
    {
        //atsisuka i desine
        if (Player.transform.position.x > transform.position.x)
        {
            if (!FacingRight)
            {
                FacingRight = true;

                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }
        }
        else // atsisuka i kaire
        if (Player.transform.position.x < transform.position.x)
        {
            if (FacingRight)
            {
                FacingRight = false;

                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }
        }
    }

    private void SwipeWithArms()
    {
        if (transform.position.x > bounx1 + .3f)
            transform.position = new Vector3(Mathf.Lerp(transform.position.x, bounx1, 0.05f), transform.position.y,
                transform.position.z);
        if (transform.position.x <= bounx1 + 0.3)
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, 0, 0.05f),
                transform.position.z);
    }

    private void TapWithPaw()
    {
        anim.SetBool("TapWithPaw", true);
    }

    private void jumpUpToTopOfScreen()
    {
        DontRotate = true;
        anim.SetBool("JumpUp", true);
    }

    public void setActiveJumpUpToTopOfScreenCollider()
    {
        jumpUpToTopOfScreenCollider.SetActive(true);
    }

    public void jumpABit()
    {
        if (Player.transform.position.x < transform.position.x) rb.AddForce(Vector3.left * 300f);
        else rb.AddForce(-Vector3.left * 300f);
    }

    public void WalkBackToCenter()
    {
        //atsisuka i desine
        if (GameManager.Instance.catOriginalPosition.x > transform.position.x)
        {
            if (!FacingRight)
            {
                FacingRight = true;
                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }
        }
        else // atsisuka i kaire
        if (GameManager.Instance.catOriginalPosition.x < transform.position.x)
        {
            if (FacingRight)
            {
                FacingRight = false;
                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }
        }

        if (transform.position.x != GameManager.Instance.catOriginalPosition.x)
        {
            transform.position = Vector3.MoveTowards(transform.position
                , new Vector3(GameManager.Instance.catOriginalPosition.x, transform.position.y, transform.position.z)
                , catMoveSpeed * Time.deltaTime);
            anim.SetBool("CatWalk", true);
        }
        else //firefly po kate NIEKUR NEINA
        {
            anim.SetBool("CatWalk", false);
            setState();
            DontRotate = false;
        }
    }

    //public void walkToCorner()
    //{
    //    //jumpUpToTopOfScreenCollider.SetActive(false);
    //    //disableColliders();

    //    var iKuriKampaNueit = 0f;
    //    //-7.55f, 7.55f
    //    if (Vector3.Distance(new Vector3(6f, 0f), new Vector3(transform.position.x, 0f)) <
    //        Vector3.Distance(new Vector3(-6f, 0f), new Vector3(transform.position.x, 0f)) && atejoPrieKampo == false)
    //    {
    //        Debug.Log("Kaire");
    //        DontRotate = true;
    //        iKuriKampaNueit = 6;
    //        anim.SetFloat("IkuriKampaNueit", iKuriKampaNueit);
    //    }
    //    else if (!atejoPrieKampo)
    //    {
    //        DontRotate = true;
    //        iKuriKampaNueit = -6;
    //        Debug.Log("Desine");
    //        anim.SetFloat("IkuriKampaNueit", iKuriKampaNueit);
    //    }

    //    if (iKuriKampaNueit != transform.position.x && !atejoPrieKampo)
    //    {
    //        anim.SetBool("CatWalk", true);
    //        transform.position = Vector3.MoveTowards(transform.position
    //            , new Vector3(iKuriKampaNueit, transform.position.y, transform.position.z)
    //            , catMoveSpeed * Time.deltaTime);

    //        if (iKuriKampaNueit < transform.position.x && FacingRight)
    //        {
    //            FacingRight = false;
    //            var scale = transform.localScale;
    //            scale = new Vector3(scale.x * -1, scale.y, scale.z);
    //            transform.localScale = scale;
    //        }
    //        else if (iKuriKampaNueit > transform.position.x && !FacingRight)
    //        {
    //            FacingRight = true;
    //            var scale = transform.localScale;
    //            scale = new Vector3(scale.x * -1, scale.y, scale.z);
    //            transform.localScale = scale;
    //        }
    //    }

    //    if (iKuriKampaNueit == transform.position.x)
    //    {
    //        atejoPrieKampo = true;
    //        DontRotate = true;
    //        anim.SetBool("WallJump", true);
    //        //teleportToCorner(iKuriKampaNueit);
    //    }
    //}

    public void teleportToCorner()
    {
        anim.SetBool("WallJump", true);
        if (transform.position.x < 0)
            transform.position = new Vector3(-10.45f, transform.position.y, transform.position.z);
        else
            transform.position = new Vector3(9.21f, transform.position.y, transform.position.z);
    }

    public void teleportBackToPlace()
    {
        if (transform.position.x > 0)
            transform.position = new Vector3(3.88f, transform.position.y, transform.position.z);
        else
            transform.position = new Vector3(-3.88f, transform.position.y, transform.position.z);
    }

    public void addForceToWallJump()
    {
        if (transform.position.x < 0)
            rb.AddForce(Vector2.left * 200f);
        else
            rb.AddForce(-Vector2.left * 200f);
    }

    private void shootFurrBall()
    {
        anim.SetBool("SpitFurrBall", true);
    }

    public void instaciateFurrBall()
    {
        var furrBall = Instantiate(FurrBall
            , new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z)
            , transform.rotation);
        Physics2D.IgnoreCollision(furrBall.GetComponent<CircleCollider2D>(), GetComponent<BoxCollider2D>());
        var moveDir = (Player.transform.position - transform.position).normalized;
        furrBall.GetComponent<Rigidbody2D>().velocity = moveDir * 20f;
    }


    public void setPositionXToWalkTo()
    {
        if (Mathf.Abs(transform.position.x) - Mathf.Abs(Player.transform.position.x) < 1.5f)
        {
            if (Mathf.Abs(maxXPos1) - Mathf.Abs(Player.transform.position.x) < 1.5f)
            {
                positionToWalkToX = Player.transform.position.x + 1.5f;
            }
            else

            if (Mathf.Abs(maxXPos2) - Mathf.Abs(Player.transform.position.x) < 1.5f)
            {
                positionToWalkToX = Player.transform.position.x - 1.5f;
            }
            else
                positionToWalkToX = Player.transform.position.x + 1.5f;
        }
        else
        {
            positionToWalkToX = Player.transform.position.x;
        }
        //positionToWalkToX = Player.transform.position.x;
        checkForPosition = false;
    }

    private void catBasicJumpUp()
    {
        DontRotate = true;
        anim.SetBool("catBasicJumpUp", true);
    }

    public void addForceToCatBasicJumpUp()
    {
        if (FacingRight)
            rb.AddForce(Vector3.right * 700f);
        else
            rb.AddForce(Vector3.left * 700f);
    }


    public void setForceToZero()
    {
        rb.velocity = Vector3.zero;
    }

    public void addForceToParashute()
    {
        rb.AddForce(Vector3.left * 1000);
    }

    public void curtainStrikeUnsucsessfullAnim()
    {
        StartCoroutine(wait1Sec());
    }
    IEnumerator wait1Sec()
    {
        yield return new WaitForSeconds(1);
        anim.SetBool("curtainLandUnsucsessfull", true);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "parashuteCornerCollider")
        {
            collision.gameObject.SetActive(false);
            climbUpWindow();
        }

        if (collision.gameObject.tag == "Player" && Current_Cat_State == CatStates.curtainStrike)
        {
            anim.SetTrigger("curtainLandSucsessfull");
            anim.SetBool("curtainStrike", false);

        }
    }


    public void walkToCornerCurtainStrike()
    {
        disableColliders();
        var iKuriKampaNueit = 0f;

        if (Vector3.Distance(new Vector3(5.5f, 0f), new Vector3(transform.position.x, 0f)) <
            Vector3.Distance(new Vector3(-5.5f, 0f), new Vector3(transform.position.x, 0f)) && atejoPrieKampoCurtain == false)
        {
            DontRotate = true;
            iKuriKampaNueit = 5.5f;
            anim.SetFloat("IkuriKampaNueit", iKuriKampaNueit);
            curtainStrikeRight = false;
        }
        else if (!atejoPrieKampoCurtain)
        {
            DontRotate = true;
            iKuriKampaNueit = -5.5f;
            anim.SetFloat("IkuriKampaNueit", iKuriKampaNueit);
            curtainStrikeRight = true;

        }

        if (iKuriKampaNueit != transform.position.x && !atejoPrieKampoCurtain)
        {
            anim.SetBool("CatWalk", true);
            transform.position = Vector3.MoveTowards(transform.position
                               , new Vector3(iKuriKampaNueit, transform.position.y, transform.position.z)
                               , catMoveSpeed * Time.deltaTime);

            if (iKuriKampaNueit < transform.position.x && FacingRight)
            {
                FacingRight = false;
                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }
            else if (iKuriKampaNueit > transform.position.x && !FacingRight)
            {
                FacingRight = true;
                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }
        }

        if (iKuriKampaNueit == transform.position.x)
        {
            inCorner = true;
            DontRotate = true;
        }
    }

}