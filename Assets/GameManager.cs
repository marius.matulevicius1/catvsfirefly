﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; private set; }

    public GameObject CatPrefab;
    private GameObject CatObject;
    public Cat cat;
    public Vector3 catOriginalPosition;
    public GameObject parashuteCornerCollider;
    public GameObject one;
    public GameObject curtainStrikeCollider;
    public GameObject wallJumpCollider1;
    public GameObject wallJumpCollider2;

    Vector3 playerOriginalPosition;

    [SerializeField]
    private GameObject RestartButton;
    [SerializeField]
    private GameObject MainMenuButton;

    [SerializeField]
    GameObject Player;

    [SerializeField]
    private GameObject joystickPanel;

    [SerializeField]
    GameObject joystick;

    private float amountOfCurrentCoins;
    SpawnBird spawnBird;
    CoinSpawning coinSpawning;

    public GameObject maxXPos1;
    public GameObject maxXPos2;

    public GameObject colliders;

    public GameObject Dog;




    private void Awake()
    {
        //catOriginalPosition = Cat.transform.position;
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public GameObject getCurtainStrikeCollider()
    {
        return curtainStrikeCollider;
    }

    public GameObject getWallStrikeCollider1()
    {
        return wallJumpCollider1;
    }

    public GameObject getWallStrikeCollider2()
    {
        return wallJumpCollider2;
    }

    IEnumerator disableOne()
    {
        yield return new WaitForSeconds(1);
        Destroy(one);
    }

    private void Start()
    {
        playerOriginalPosition = Player.transform.position;
        spawnBird = gameObject.GetComponent<SpawnBird>();
        coinSpawning = gameObject.GetComponent<CoinSpawning>();
        startGame();
        StartCoroutine(disableOne());
    }

    public void startGame()
    {
        Player.SetActive(true);
        CatObject = Instantiate(CatPrefab, catOriginalPosition, transform.rotation);
        cat = CatObject.GetComponent<Cat>();
        spawnBird.enabled = true;
        coinSpawning.enabled = true;
        joystickPanel.SetActive(true);

    }

    public void endGame()
    {
        Player.transform.position = playerOriginalPosition;
        Player.SetActive(false);
        Destroy(CatObject);
        spawnBird.enabled = false;
        coinSpawning.enabled = false;
        joystickPanel.SetActive(false);


        Destroy( GameObject.FindGameObjectWithTag("Coin"));
    }


    public void GameOver()
    {
        cat.Current_Cat_State = Cat.CatStates.GameOver;
        RestartButton.SetActive(true);
        MainMenuButton.SetActive(true);
        joystickPanel.SetActive(false);
        spawnBird.enabled = false;
        disableColliders();
    }

    public void Restart() { 
        Destroy(CatObject);
        CatObject = Instantiate(CatPrefab,catOriginalPosition, transform.rotation);
        cat = CatObject.GetComponent<Cat>();
        Player.transform.position = playerOriginalPosition;
        Player.SetActive(true);
        Player.GetComponent<cameraFollowPlayer>().ResetCameraPosition();
        RestartButton.SetActive(false);
        MainMenuButton.SetActive(false);
        joystickPanel.SetActive(true);
        spawnBird.enabled = true;



        enableColliders();

    }

    void disableColliders()
    {
        colliders.SetActive(false);
    }

    void enableColliders()
    {
        colliders.SetActive(true);
    }


    public void pickUpCoin()
    {
        float coins = PlayerPrefs.GetFloat("Coins");
        coins += 10;
        PlayerPrefs.SetFloat("Coins", coins);
    }

    public GameObject getCatObject()
    {
        return CatObject;
    }

    public GameObject getPlayerObject()
    {
        return Player;
    }

    public GameObject getParashuteCornerCollider()
    {
        return parashuteCornerCollider;
    }

    public float getMaxXPos1()
    {
        return maxXPos1.transform.position.x;
    }

    public float getMaxXPos2()
    {
        return maxXPos2.transform.position.x;
    }

    public void MainMenuButtonClick()
    {
        SceneManager.LoadScene("fromPlayToMainMenu");
    }
}
