﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gentleCameraMovement : MonoBehaviour
{

    public Transform cameraTransform;
    public float shakeAmount = 3;
    public float shakeSpeed = 20;
    Vector3 originalPos;
    Vector3 newPos;

    void OnEnable()
    {
        originalPos = cameraTransform.position;
        newPos = cameraTransform.position;
    }

    void Update()
    {
        Shake();
    }

    public void Shake()
    {
        if (Vector3.Distance(newPos, cameraTransform.position) <= shakeAmount / 30) { newPos = originalPos + Random.insideUnitSphere * shakeAmount; }
        cameraTransform.position = Vector3.MoveTowards(cameraTransform.position, newPos, shakeSpeed * Time.deltaTime);
    }
}
