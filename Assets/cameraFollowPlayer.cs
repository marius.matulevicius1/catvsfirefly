﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollowPlayer : MonoBehaviour {

    public GameObject mainCamera;
    Vector3 cameraOrigPos;
    public virtualJoystick joystick;
    [Space]
    public GameObject layer2;
    public GameObject layer22;

    public GameObject layer3;

    public GameObject layer4;
    public GameObject layer44;


    float moveSpeed;
    [Space]
    public float layer2MoveSpeed;
    public float layer3MoveSpeed;
    public float layer4MoveSpeed;
    public float cameraMoveSpeed;


    // Use this for initialization
    void Start () {
        moveSpeed = gameObject.GetComponent<Firefly>().moveSpeed;
        cameraOrigPos = mainCamera.transform.position;
    }



    // Update is called once per frame
    void Update () {
        cameraMovement();
        layerMovement();
	}

    private void cameraMovement()
    {
        if (transform.position.x > -9.14f && transform.position.x < 8.06f)
        {
            float horizontalX = setHorizontalX();


            Vector3 moveVec = new Vector2(horizontalX, 0);
            mainCamera.transform.Translate(moveVec * moveSpeed * Time.deltaTime / 5);

            mainCamera.transform.position = new Vector3(Mathf.Clamp(mainCamera.transform.position.x, -2.15f, 1.02f), mainCamera.transform.position.y, mainCamera.transform.position.z);
        }
    }

    float setHorizontalX()
    {
        float MoveVecX;

        if (joystick.Horizontal() < 0)
        {
            MoveVecX = -1;
        }
        else
        if (joystick.Horizontal() > 0)
        {
            MoveVecX = 1;
        }
        else
            MoveVecX = 0;

        return MoveVecX;
    }

    void layerMovement()
    {
        if (transform.position.x > -9.12f && transform.position.x < 8f)
        {
            float horizontalX = setHorizontalX();

            Vector3 moveVec = new Vector2(horizontalX, 0);
            mainCamera.transform.Translate(moveVec * moveSpeed * Time.deltaTime / cameraMoveSpeed);

            layer2.transform.Translate(-moveVec * moveSpeed * Time.deltaTime / layer2MoveSpeed);
            layer22.transform.Translate(-moveVec * moveSpeed * Time.deltaTime / layer2MoveSpeed);

            layer3.transform.Translate(-moveVec * moveSpeed * Time.deltaTime / layer3MoveSpeed);

            layer4.transform.position += (-moveVec * moveSpeed * Time.deltaTime / layer4MoveSpeed);
            layer44.transform.Translate(-moveVec * moveSpeed * Time.deltaTime / layer4MoveSpeed);
        }
    }


    public void ResetCameraPosition()
    {
        mainCamera.transform.position = cameraOrigPos;
    }
}
